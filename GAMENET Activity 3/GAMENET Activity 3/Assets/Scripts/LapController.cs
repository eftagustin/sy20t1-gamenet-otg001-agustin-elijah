﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;
using UnityEngine.UI;

public class LapController : MonoBehaviourPunCallbacks
{
    private int finishOrder;
    public List<GameObject> lapTriggers = new List<GameObject>();
    string nickname;

    public enum RaiseEventsCode
    {
        WhoFinishedEventCode = 0
    }
    // Start is called before the first frame update
    void Start()
    {
        foreach(GameObject go in RacingGameManager.instance.lapTriggers)
        {
            lapTriggers.Add(go);
        }

        nickname = photonView.Owner.NickName;
    }

    private void OnTriggerEnter(Collider col)
    {
        if (lapTriggers.Contains(col.gameObject))
        {
            int indexTrigger = lapTriggers.IndexOf(col.gameObject);
            lapTriggers[indexTrigger].SetActive(false);
        }

        if(col.gameObject.tag == "FinishTrigger")
        {
            //Game Finish here
            Debug.Log("We have a winner!");
            GameFinish();
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnEnable()
    {
        PhotonNetwork.NetworkingClient.EventReceived += OnEvent;
    }

    private void OnDisable()
    {
        PhotonNetwork.NetworkingClient.EventReceived -= OnEvent;
    }

    private void OnEvent(EventData photonEvent)
    {

        //refers to event byte 0
        if (photonEvent.Code == (byte)RaiseEventsCode.WhoFinishedEventCode)
        {
            object[] data = (object[])photonEvent.CustomData;
            string nickNameOfFinishPlayer = (string)data[0];
            finishOrder = (int)data[1];

            int viewId = (int)data[2];



            Debug.Log(nickNameOfFinishPlayer + " " + finishOrder);


            GameObject orderUiText = RacingGameManager.instance.FinisherTextUI[finishOrder - 1];
            orderUiText.SetActive(true);

            if (viewId == photonView.ViewID)
            {
                orderUiText.GetComponent<Text>().text = finishOrder + " " + nickname + "(YOU)";
                orderUiText.GetComponent<Text>().color = Color.blue;
            }
            else
            {
                orderUiText.GetComponent<Text>().text = finishOrder + " " + nickname;
            }


        }
    }

    public void GameFinish() {
        {
            GetComponent<PlayerSetup>().camera.transform.parent = null;
            GetComponent<VehicleMovement>().enabled = false;
            nickname = photonView.Owner.NickName;
            int viewId = photonView.ViewID;

            finishOrder++;

            //send here
            object[] data = new object[] {nickname, finishOrder, viewId};

            RaiseEventOptions raiseEventOptions = new RaiseEventOptions
            {
                Receivers = ReceiverGroup.All,
                CachingOption = EventCaching.AddToRoomCache
            };

            SendOptions sendOptions = new SendOptions
            {
                Reliability = false
            };  

            PhotonNetwork.RaiseEvent((byte) RaiseEventsCode.WhoFinishedEventCode, data, raiseEventOptions,sendOptions);
        }
    }
}

