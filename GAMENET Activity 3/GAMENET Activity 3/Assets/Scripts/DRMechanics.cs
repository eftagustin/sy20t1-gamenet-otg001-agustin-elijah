﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;
using UnityEngine.UI;
using UnityEngine.Events;

public class DRMechanics : MonoBehaviourPunCallbacks
{
    //private int finishOrder;
    //public List<GameObject> lapTriggers = new List<GameObject>();
    string nickname;
    public float health;
    public float startHealth = 100;
    public GameObject projectile;
    public Transform firePos;
    public Image healthBar;
    public Camera camera;
    //public Camera camera2;

    //public Image aaa;
    public Text whoKilled;
    private string winner;
    private string shooter;
    private string killer;
    public int playersLeft;

    public enum RaiseEventsCode
    {
        WhoFinishedEventCode = 0

    }
    // Start is called before the first frame update
    void Start()
    {
        health = startHealth;
        camera = camera.GetComponentInChildren<Camera>();
        playersLeft = DeathRaceManager.instance.playersLeft;
        whoKilled = DeathRaceManager.instance.whoKilledRef;
        whoKilled.text = " ";
     
        // foreach (GameObject go in RacingGameManager.instance.lapTriggers)
        // {
        //     lapTriggers.Add(go);
        // }

        //nickname = photonView.Owner.NickName;
    }

    private void OnTriggerEnter(Collider col)
    {
        if(col.tag == "projectile")
        {
            photonView.RPC("TakeDamage", RpcTarget.AllBuffered, 15);
            Destroy(col);
        }

    }

    [PunRPC]
    public void FireProjectile(PhotonMessageInfo info)
    {
        shooter = info.Sender.NickName;
        GameObject p = PhotonNetwork.Instantiate(projectile.name, firePos.position, Quaternion.identity);
        p.GetComponent<Rigidbody>().AddForce(firePos.transform.forward * 500);
        Destroy(p, 3.0f);
        
    }

    [PunRPC]
    public void FireLaser(PhotonMessageInfo info)
    {
        shooter = info.Sender.NickName;

        Debug.Log("firing laser..");
        RaycastHit hit;
        Ray ray = camera.ViewportPointToRay(new Vector3(0.5f, 0.5f));

        if (Physics.Raycast(ray, out hit, 200))
        {
            Debug.Log(hit.collider.gameObject.name);
            //photonView.RPC("CreateHitEffects", RpcTarget.All, hit.point);

            if (hit.collider.gameObject.CompareTag("Player") && !hit.collider.gameObject.GetComponent<PhotonView>().IsMine)
            {
                hit.collider.gameObject.GetComponent<PhotonView>().RPC("TakeDamage", RpcTarget.AllBuffered, 15);
            }

            //CheckWin(hit.collider.gameObject);
        }
    }

    [PunRPC]
    public void TakeDamage(int damage, PhotonMessageInfo info)
    {
        this.health -= damage;
        this.healthBar.fillAmount = health / startHealth;

        if (health <= 0)
        {
            //call PUN EVENT on my death
            nickname = photonView.Owner.NickName;
            int viewId = photonView.ViewID;

            //send here
            object[] data = new object[] { nickname, shooter, viewId };

            RaiseEventOptions raiseEventOptions = new RaiseEventOptions
            {
                Receivers = ReceiverGroup.All,
                CachingOption = EventCaching.AddToRoomCache
            };

            SendOptions sendOptions = new SendOptions
            {
                Reliability = false
            };

            PhotonNetwork.RaiseEvent((byte)RaiseEventsCode.WhoFinishedEventCode, data, raiseEventOptions, sendOptions);

            photonView.RPC("AssignShooter", RpcTarget.AllBuffered, info.Sender.NickName);
            //Destroy(gameObject, 3.0f);
            //GameFinishCheck();
        }

        //Destroy(gameObject, 3.0f);
        //who killed who
        //Debug.Log(info.Sender.NickName + " has killed " + info.photonView.Owner.NickName);

        //killCount++;

 
        
    }

    [PunRPC]
    public void AssignShooter(string n_shooter)
    {
        shooter = n_shooter;
        winner = shooter;
        GameFinishCheck(shooter);
    }

    // Update is called once per frame
    void Update()
    {
        if (photonView.IsMine)
        {
            //        if (Input.GetKeyUp(KeyCode.Z))
            if (Input.GetMouseButtonDown(0))
            {

                Debug.Log("fired laser");
                //FireLaser();
                photonView.RPC("FireLaser", RpcTarget.AllBuffered);
                //FireLaser();

            }
            //else if (Input.GetKeyUp(KeyCode.X))
            else if (Input.GetMouseButtonDown(1))
            {
                Debug.Log("fired proj");
                //FireProjectile();
                photonView.RPC("FireProjectile", RpcTarget.AllBuffered);
                //FireProjectile();
            }
        }

        if (Input.GetKeyDown(KeyCode.P) && photonView.IsMine)
        {
            photonView.RPC("LoadLobby", RpcTarget.AllBuffered);
        }
        if (Input.GetKeyDown(KeyCode.O) && photonView.IsMine)
        {
            GoLobby();
        }
    }

    private void GoLobby()
    {
        PhotonNetwork.LoadLevel("LobbyScene");
        Destroy(this.gameObject);
    }

    [PunRPC]
    private void LoadLobby()
    {
        PhotonNetwork.LoadLevel("LobbyScene");
        Destroy(this.gameObject);
    }

    private void OnEnable()
    {
        PhotonNetwork.NetworkingClient.EventReceived += OnEvent;
    }

    private void OnDisable()
    {
        PhotonNetwork.NetworkingClient.EventReceived -= OnEvent;
    }

    private void OnEvent(EventData photonEvent)
    {

        if (photonEvent.Code == (byte)RaiseEventsCode.WhoFinishedEventCode)
        {
            object[] data = (object[])photonEvent.CustomData;
            string nickNameOfKilled = (string)data[0];
            //killer = (string)data[1];

            //int viewId = (int)data[2];

            whoKilled.text = nickNameOfKilled + " was killed by " + shooter + " !";

            StartCoroutine(ClearText());
            //Debug.Log(nickNameOfFinishPlayer + " " + finishOrder);


            //GameObject orderUiText = RacingGameManager.instance.FinisherTextUI[finishOrder - 1];
            //orderUiText.SetActive(true);

            //if (viewId == photonView.ViewID)
            //{
            //    orderUiText.GetComponent<Text>().text = finishOrder + " " + nickname + "(YOU)";
            //    orderUiText.GetComponent<Text>().color = Color.blue;
            //}
            //else
            //{
            //    orderUiText.GetComponent<Text>().text = finishOrder + " " + nickname;
            //}


        }

    }

    private IEnumerator ClearText()
    {
        yield return new WaitForSeconds(4.0f);

        whoKilled.text = " ";
    }

    public void GameFinishCheck(string winner)
    {

        playersLeft--;
 
        if (playersLeft == 1)
        {
          DeathRaceManager.instance.timeText.text = "The winner is " + winner;
        }

        //whoKilled.text = info.Sender.NickName + " has killed " + info.photonView.Owner.NickName;

        //StartCoroutine(ResetKillUI());
    }
}

