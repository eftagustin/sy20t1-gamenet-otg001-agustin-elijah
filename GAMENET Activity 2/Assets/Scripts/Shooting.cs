﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;

public class Shooting : MonoBehaviourPunCallbacks
{

    public Camera camera;
    public GameObject hitEffectPrefab;

    [Header("HP stuff")]
    public float startHealth = 100;
    private float health;
    public Image healthBar;
    private Animator animator;
    private Text whoKilled;
    public int killCount;
    private string winner;

    void Start()
    {
        health = startHealth;
        healthBar.fillAmount = health / startHealth;
        animator = this.GetComponent<Animator>();
        whoKilled = GameObject.Find("WhoKilledText").GetComponent<Text>();
    }


    void Update()
    {

    }

    public void Fire()
    {
        RaycastHit hit;
        Ray ray = camera.ViewportPointToRay(new Vector3(0.5f, 0.5f));

        if (Physics.Raycast(ray, out hit, 200))
        {
            Debug.Log(hit.collider.gameObject.name);
            photonView.RPC("CreateHitEffects", RpcTarget.All, hit.point);

            if (hit.collider.gameObject.CompareTag("Player") && !hit.collider.gameObject.GetComponent<PhotonView>().IsMine)
            {
                hit.collider.gameObject.GetComponent<PhotonView>().RPC("TakeDamage", RpcTarget.AllBuffered, 25);
            }

            CheckWin(hit.collider.gameObject);
        }
    }

    void CheckWin(GameObject target) {
        float targetHealth = target.GetComponent<Shooting>().health;

        if (targetHealth > 0 && (targetHealth - 25) <= 0)
        {
            killCount++; 
        }
    }

    [PunRPC]
    public void TakeDamage(int damage, PhotonMessageInfo info)
    {
        this.health -= damage;
        this.healthBar.fillAmount = health / startHealth;

        if (health <= 0)
        {
            Die();

            //who killed who
            Debug.Log(info.Sender.NickName + " has killed " + info.photonView.Owner.NickName);

            killCount++;

            if(killCount >= 2)
            {
                winner = info.Sender.NickName;
            }

            whoKilled.text = info.Sender.NickName + " has killed " + info.photonView.Owner.NickName +
              " and their current killCount is " + killCount;

            StartCoroutine(ResetKillUI());
        }
    }

    IEnumerator ResetKillUI()
    {
        yield return new WaitForSeconds(4.0f);
        whoKilled.text = " ";

        if (killCount >= 2)
        {
            photonView.RPC("WinAnnounce", RpcTarget.AllBuffered);
            yield return new WaitForSeconds(5f);
        }
    }

    [PunRPC]
    public void WinAnnounce()
    {
        
        whoKilled.text = winner + " has won the game! Redirecting in a few...";
        StartCoroutine(ReturnLobby());
    }

    IEnumerator ReturnLobby()
    {
        yield return new WaitForSeconds(5f);

        whoKilled.text = "Returning to lobby.";

        yield return new WaitForSeconds(3f);

        photonView.RPC("LoadLobby", RpcTarget.AllBuffered);

        //PhotonNetwork.LoadLevel("LobbyScene");
    }

    [PunRPC]
    public void LoadLobby()
    {
        Debug.Log("RPC Load Lobby called");
        
        PhotonNetwork.LoadLevel("LobbyScene");
    }

    public void Die()
    {
        if (photonView.IsMine)
        {
            animator.SetBool("isDead", true);
            StartCoroutine(RespawnCountdown());
        }

        //StartCoroutine(RespawnCountdown());
    }

    IEnumerator RespawnCountdown()
    {
        GameObject respawnText = GameObject.Find("RespawnText");
        float respawnTime = 5.0f;

        while(respawnTime > 0)
        {
            yield return new WaitForSeconds(1.0f);
            respawnTime--;

            transform.GetComponent<PlayerMovementController>().enabled = false;
            respawnText.GetComponent<Text>().text = "You were killed. Respawn in " + respawnTime.ToString(".00");
        }

        animator.SetBool("isDead", false);
        respawnText.GetComponent<Text>().text = " ";

        //int randPointX = Random.Range(-20, 20);
        //int randPointZ = Random.Range(-20, 20);

        transform.position = SpawnMgr.instance.spawnPoints[Random.Range(0, 4)].transform.position;
        //this.transform.position = new Vector3(randPointX, 0, randPointZ);
        transform.GetComponent<PlayerMovementController>().enabled = true;

        photonView.RPC("RegainHealth", RpcTarget.AllBuffered);
;    }

    [PunRPC]
    public void RegainHealth()
    {
        health = 100;
        healthBar.fillAmount = health / startHealth;
    }

    [PunRPC]
    public void CreateHitEffects(Vector3 position)
    {
        GameObject hitEffectGameObject = Instantiate(hitEffectPrefab, position, Quaternion.identity);
        Destroy(hitEffectGameObject, 0.2f);
    }
}
