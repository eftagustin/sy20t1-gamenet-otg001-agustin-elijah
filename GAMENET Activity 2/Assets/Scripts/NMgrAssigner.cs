﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NMgrAssigner : MonoBehaviour
{
    [Header("Login UI Panel")]
    public InputField playerNameInput;

    [Header("Connection Status Panel")]
    public Text connectionStatusText;

    [Header("LoginUI Panel")]
    public GameObject loginUiPanel;

    [Header("Game Options Panel")]
    public GameObject gameOptionsPanel;

    [Header("Create Room Panel")]
    public GameObject createRoomPanel;
    public InputField roomNameInputField;
    public InputField playerCountInputField;

    [Header("Join Random Room Panel")]
    public GameObject joinRandomRoomPanel;

    [Header("Show Game Room List Panel")]
    public GameObject showGameRoomListPanel;

    [Header("Inside Room Panel")]
    public GameObject insideRoomPanel;
    public Text roomInfoText;
    public GameObject playerListItemPrefab;
    public GameObject playerListViewParent;
    public GameObject startGameButton;

    [Header("Room List Panel")]
    public GameObject roomListPanel;
    public GameObject roomItemPrefab;
    public GameObject roomListParent;

    void Awake()
    {
        NetworkManager.instance.playerNameInput = playerNameInput;
        NetworkManager.instance.connectionStatusText = connectionStatusText;
        NetworkManager.instance.loginUiPanel = loginUiPanel;
        NetworkManager.instance.gameOptionsPanel = gameOptionsPanel;
        NetworkManager.instance.createRoomPanel = createRoomPanel;
        NetworkManager.instance.roomNameInputField = roomNameInputField;
        NetworkManager.instance.playerCountInputField = playerCountInputField;
        NetworkManager.instance.joinRandomRoomPanel = joinRandomRoomPanel;
        NetworkManager.instance.showGameRoomListPanel = showGameRoomListPanel;
        NetworkManager.instance.insideRoomPanel = insideRoomPanel;
        NetworkManager.instance.roomInfoText = roomInfoText;
        NetworkManager.instance.playerListItemPrefab = playerListItemPrefab;
        NetworkManager.instance.playerListViewParent = playerListViewParent;
        NetworkManager.instance.startGameButton = startGameButton;
        NetworkManager.instance.roomListPanel = roomListPanel;
        NetworkManager.instance.roomItemPrefab = roomItemPrefab;
        NetworkManager.instance.roomListParent = roomListParent;
    }
}
