﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;

public class TakingDamage : MonoBehaviourPunCallbacks
{

    private float startHealth = 100;
    public float health;

    [SerializeField]Image healthBar;

    void Start()
    {
        health = startHealth;
        healthBar.fillAmount = health / startHealth;
    }

    void Update()
    {

    }

    //remote procedufre call
    //method calls on remote clients on the same room
    [PunRPC] public void TakeDamage(int damage)
    {
        health -= damage;
        healthBar.fillAmount = health / startHealth;
        Debug.Log(health);

        if(health <= 0)
        {
            Die();
        }
    }

    private void Die()
    {
        if (photonView.IsMine)
        {
            GameManager.instance.LeaveRoom();
        }
    }
}
